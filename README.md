[![Build Status](https://travis-ci.org/MetaModels/notelist.png)](https://travis-ci.org/MetaModels/core)
[![Latest Version tagged](http://img.shields.io/github/tag/MetaModels/notelist.svg)](https://github.com/MetaModels/notelist/tags)
[![Latest Version on Packagist](http://img.shields.io/packagist/v/MetaModels/notelist.svg)](https://packagist.org/packages/MetaModels/notelist)
[![Installations via composer per month](http://img.shields.io/packagist/dm/MetaModels/notelist.svg)](https://packagist.org/packages/MetaModels/notelist)
[![Dependency Status](https://www.versioneye.com/php/metamodels:notelist/badge.svg)](https://www.versioneye.com/php/metamodels:notelist)

# MetaModels note list

This is the MetaModels note list implementation.

# Installation

`composer require metamodels/notelist` 

Docs:
-----------

* [The official MetaModel Documentation (en)](http://metamodels.readthedocs.org/en/latest/index.html)
* [The official MetaModel Documentation (de)](http://metamodels.readthedocs.org/de/latest/index.html)

Feel free to contribute the MetaModel Documentation in [EN](https://github.com/MetaModels/docs)
or [DE](https://github.com/MetaModels/docs-de)

Ressources:
-----------

* [MetaModels Website](https://now.metamodel.me)
* [MetaModels Contao Wiki [DE]](http://de.contaowiki.org/MetaModels)
* [MetaModels Contao Community sub forum [DE]](https://community.contao.org/de/forumdisplay.php?149-MetaModels)
* [MetaModels IRC Channel on freenode #contao.mm](irc://chat.freenode.net/#contao.mm)
