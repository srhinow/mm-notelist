<?php

/**
 * This file is part of MetaModels/notelist.
 *
 * (c) 2017 The MetaModels team.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package    MetaModels
 * @author     Christian Schiffler <c.schiffler@cyberspectrum.de>
 * @author     Ingolf Steinhardt <info@e-spin.de>
 * @copyright  2017 The MetaModels team.
 * @license    https://github.com/MetaModels/notelist/blob/master/LICENSE LGPL-3.0
 * @filesource
 */

declare(strict_types = 1);

$GLOBALS['TL_LANG']['BRD']['metamodel_notelist']        = 'Note lists for %s';
$GLOBALS['TL_LANG']['MSC']['metamodel_notelist_add']    = 'Add to %s';
$GLOBALS['TL_LANG']['MSC']['metamodel_notelist_remove'] = 'Remove from %s';
$GLOBALS['TL_LANG']['MSC']['metamodel_notelist_edit']   = 'Edit by %s';

$GLOBALS['TL_LANG']['MSC']['metamodel_notelist_display_backend'] = 'MetaModels note list for MetaModel: %1$s';
