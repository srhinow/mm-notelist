<?php

/**
 * This file is part of MetaModels/notelist.
 *
 * (c) 2017 The MetaModels team.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package    MetaModels
 * @author     Christian Schiffler <c.schiffler@cyberspectrum.de>
 * @copyright  2017 The MetaModels team.
 * @license    https://github.com/MetaModels/notelist/blob/master/LICENSE LGPL-3.0
 * @filesource
 */

declare(strict_types = 1);

$GLOBALS['TL_LANG']['tl_metamodel_notelist']['new'][0]            = 'New note list';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['new'][1]            = 'Create new note list';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['edit'][0]           = 'Edit note list';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['edit'][1]           = 'Edit note list ID %s';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['cut'][0]            = 'Cut note list';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['cut'][1]            = 'Cut note list ID %s';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['copy'][0]           = 'Copy note list';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['copy'][1]           = 'Copy note list ID %s';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['delete'][0]         = 'Delete note list';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['delete'][1]         = 'Delete note list ID %s';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['show'][0]           = 'Attribute details';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['show'][1]           = 'Show details of note list ID %s';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['name'][0]           = 'Note list name';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['name'][1]           = 'Enter the name for this note list.';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['name_langcode'][0]  = 'Language';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['name_langcode'][1]  = 'The language.';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['name_value'][0]     = 'Note list name';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['name_value'][1]     = 'The name of this note list in the given language.';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['config_legend']     = 'Configuration';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['storageAdapter'][0] = 'Storage adapter';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['storageAdapter'][1] = 'Choose the storage adapter to use.';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['filter'][0]         = 'Filter';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['filter'][1]         = 'Choose the filter setting to use for item acceptance.';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['form'][0]           = 'Form';
$GLOBALS['TL_LANG']['tl_metamodel_notelist']['form'][1]           = 'Choose the form to use for meta data handling (if none selected, simple links for add/remove will get used).';
